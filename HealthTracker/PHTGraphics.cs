﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthTracker
{
    public partial class PHTGraphics : Form
    {
        private Bitmap memoryImage;
        public PHTGraphics()
        {
            InitializeComponent();
            //We need to add error checking for the instance if the database is empty (Because this blows up if
            //There's not data in it and you try to bind data to a graph. 

            List<HealthInfo> dataset = new WebConn().getData(MainForm.username, MainForm.startdate, MainForm.enddate);

           
            if (dataset.Count > 0)
            {
                var result =
                    from x in dataset
                    select new {
                        datetime = DateTime.FromFileTime(x.datetime).ToShortDateString()
                        , x.cardioHours
                        , x.strengthTrainingHours
                        , x.workHours
                        , x.sleptHours
                        , x.pulse
                        , x.bloodPressureSys
                        , x.bloodPressureDys
                        , x.weight
                        , x.calories
                        , x.bodyMassIndex};

                this.dataGridView1.DataSource = result.ToList();

                var chart1Result =
                     from x in dataset 
                     select new { datetime = DateTime.FromFileTime(x.datetime).ToShortDateString(), x.cardioHours, x.sleptHours, x.strengthTrainingHours, x.workHours }; 
               
                this.chart1.DataBindTable(chart1Result.ToList(), "datetime");

                var pieResult =
                    from x in dataset
                    select new
                    {
                        workHours = dataset.Sum(w => w.workHours)
                        ,cardioHours = dataset.Sum(c => c.cardioHours)
                        ,sleptHours = dataset.Sum(s => s.sleptHours)
                        ,strengthTrainingHours = dataset.Sum(s => s.strengthTrainingHours)
                    };

                DataTable dt = new DataTable();
                dt.Columns.Add("Type");
                dt.Columns.Add("Hours");

                dt.Rows.Add("work", pieResult.First().workHours);
                dt.Rows.Add("cardio", pieResult.First().cardioHours);
                dt.Rows.Add("slept", pieResult.First().sleptHours);
                dt.Rows.Add("strength", pieResult.First().strengthTrainingHours);

                this.pieChart.DataSource = (dt);
                this.pieChart.Series["Series1"].XValueMember = "Type";
                this.pieChart.Series["Series1"].YValueMembers = "Hours";
                this.pieChart.DataBind();


                var barResult =
                    from x in dataset
                    select new { datetime = DateTime.FromFileTime(x.datetime).ToShortDateString(), x.weight };

                this.barChart.DataBindTable(barResult.ToList());



            }//end of has valid count

            /*
            int count = (from x in db.data where x.username == MainForm.username select x.datetime).Count();

            Console.WriteLine("Count was: {0}", count);
            if (count > 0) //Check to make sure we have objects!
            {
                var tableResult = 
                    from x in db.data 
                    where x.username.Equals(MainForm.username) 
                    select x;

                this.dataGridView1.DataSource = tableResult.ToList();

                var chart1Result =
                    from x in db.data //db is the context i created above, data is the data Table, and x is arbitrary, can be whatevs
                    where x.username == MainForm.username //Here is where statement Nice because you can use c# sytnax like && and || :)
                    select new { x.datetime, x.cardioHours, x.sleptHours, x.strengthTrainingHours, x.workHours };   //Now normally you coulld just do a
                                                                                                        //a 'select x.etc' but since we're selecting only
                                                                                                        //certain things we say select new (which is called
                                                                                                        //projection fyi. 
                this.chart1.DataBindTable(chart1Result.ToList(), "datetime"); // Oh and here we must make sure it is in a list. Or It gets mad and yells at us. 

                var pieResult =
                    from x in db.data
                    where x.username == MainForm.username
                    select new
                    {
                        workHours = db.data.Sum(w => w.workHours)
                        ,
                        cardioHours = db.data.Sum(c => c.cardioHours)
                        ,
                        sleptHours = db.data.Sum(s => s.sleptHours)
                        ,
                        strengthTrainingHours = db.data.Sum(s => s.strengthTrainingHours)
                    };

                DataTable dt = new DataTable();
                dt.Columns.Add("Type");
                dt.Columns.Add("Hours");

                dt.Rows.Add("work", pieResult.First().workHours);
                dt.Rows.Add("cardio", pieResult.First().cardioHours);
                dt.Rows.Add("slept", pieResult.First().sleptHours);
                dt.Rows.Add("strength", pieResult.First().strengthTrainingHours);

                this.pieChart.DataSource = (dt);
                this.pieChart.Series["Series1"].XValueMember = "Type";
                this.pieChart.Series["Series1"].YValueMembers = "Hours";
                this.pieChart.DataBind();
            
            }//end of had valid data statement
            */


        }//end of constructor

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            CaptureScreen();
            printDialog1.Document = printDocument1;
            printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
            DialogResult result = printDialog1.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                printDocument1.Print();
            }

        }//end of Graphics CTOR

        Bitmap CaptureScreen()
        {
            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);

            return memoryImage;
        }

        private void printDocument1_PrintPage(System.Object sender,
            System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(memoryImage, 0, 0);
        }

    }//end of grpahics class
}
