﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace HealthTracker
{
    public partial class LoginForm : Form
    {
        private int attempts = 0;
        private Boolean valid = false;
        public static String reusername { get; set; }

        public LoginForm()
        {
            InitializeComponent();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String username = this.usernameTextBox.Text ?? "";
            String password = this.passwordTextBox.Text ?? "";

            if (username == "")
            {
                MessageBox.Show("The username cannot be left empty.");
                return;
            }

            User usr;

            valid = new WebConn().validate(username, password);
            /*localdb code:
            using (var web = new WebClient())
            {
                string result = web.DownloadString(VALIDATE_USER + "?username=" + username + "&password=" + password);
                if (result == "true") valid = true;
                else valid = false;
            }
             */
            
            if (valid)
            {
                Program.username = username;
                Program.authorized = true;
                this.Close();
            }
            else
            {
                attempts++;
                MessageBox.Show("The Username or Password entered is not correct.\nAttempt: " + attempts);
            }

        }

        private void newButton_Click(object sender, EventArgs e)
        {
            new NewProfileForm().ShowDialog(this);
            if (reusername != "")
            {
                this.usernameTextBox.Text = reusername;
                this.passwordTextBox.Text = "";
                this.passwordTextBox.Focus();
            }

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            //Need to remember to check the database for the Automatic login and if so, then skip this and go to MainForm
        }
    }
}
