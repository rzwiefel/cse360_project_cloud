﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace HealthTracker
{
    class WebConn
    {
        //Class Vars
        static int objectCount = 0;
        private const string baseURL = "http://ryanz.us:5000"; 
        private const string onlineURL = "/online";
        private const string dataGetURL = "/data/get";         //TODO
        private const string dataPutURL = "/data/add";
        private const string userValidateURL = "/user/validate";
        private const string userAddURL = "/user/add";
        private const string userExistsURL = "/user/exists";
        private const string dataDeleteURL = "/data/delete";

        public WebConn()
        {
            objectCount++;
        }//CTOR

        ~WebConn()
        {
            objectCount--;
        }//DCTOR

        public Boolean isOnline()
        {
            Boolean online = false;
            using (var web = new WebClient())
            {
                try
                {
                    string result = web.DownloadString(baseURL + onlineURL);
                    if (result == "true") online = true;
                }
                catch (TimeoutException e)
                {
                    online = false;
                    Console.WriteLine("Connection Timed out.");
                }
            }

            return online;
        }//end of isOnline

        public Boolean validate(string username, string password)
        {
            Boolean valid = false;
            using (var web = new WebClient())
            {
                try
                {
                    string result = web.DownloadString(baseURL + userValidateURL + "?username=" + username + "&password=" + password);
                    if (result == "true") valid = true;
                    else valid = false;
                }
                catch (TimeoutException e)
                {
                    Console.WriteLine("Connection Timed out");
                    valid = false;
                    MessageBox.Show("We couldn't connect to the server. Make sure you're connected to the internet and try again.");
                }
            }

            return valid;
        }//end of validate

        public Boolean addUser(string jsonUser)
        {
            Boolean success = false;
            using (var web = new WebClient())
            {
                string result = web.UploadString(baseURL + userAddURL, jsonUser);
                if (result == "true") success = true;
            }

            return success;
        }//end of addUser

        public Boolean exists(string username)
        {
            Boolean exists = false;
            using (var web = new WebClient())
            {
                string result = web.DownloadString(baseURL + userExistsURL + "/" + username);
                if (result == "true") exists = true;
                else exists = false;
            }

            return exists;
        }//end of validate

        public Boolean addData(string jsonData)
        {
            Boolean success = false;
            using (var web = new WebClient())
            {
                string result = web.UploadString(baseURL + dataPutURL, jsonData);
                if (result == "true") success = true;
            }

            return success;
        }//end of addData

        public List<HealthInfo> getData(string username)
        {
            List<HealthInfo> result = new List<HealthInfo>();
            string resultString = "";
            using (var web = new WebClient())
            {
                try
                {
                    resultString = web.DownloadString(baseURL + dataGetURL + "?username=" + username);
                    result = JsonConvert.DeserializeObject<List<HealthInfo>>(resultString);
                }
                catch (TimeoutException e)
                {
                    Console.WriteLine("Connection Timed out");
                    MessageBox.Show("We couldn't connect to the server. Make sure you're connected to the internet and try again.");
                }

            }

            return result;
        }//end of getData

        public List<HealthInfo> getData(string username, long startdate, long enddate)
        {
            List<HealthInfo> result = new List<HealthInfo>();
            string resultString = "";
            using (var web = new WebClient())
            {
                try
                {
                    resultString = web.DownloadString(baseURL + dataGetURL + "?username=" + username + "&startdate=" + startdate + "&enddate=" + enddate);
                    result = JsonConvert.DeserializeObject<List<HealthInfo>>(resultString);
                }
                catch (TimeoutException e)
                {
                    Console.WriteLine("Connection Timed out");
                    MessageBox.Show("We couldn't connect to the server. Make sure you're connected to the internet and try again.");
                }

            }

            return result;
        }//end of getData Overloaded 

        public Boolean delete(string username)
        {
            Boolean success = false;
            using (var web = new WebClient())
            {
                try
                {
                    string result = web.DownloadString(baseURL + dataDeleteURL + "/" + username);
                    if (result == "true") success = true;
                    else success = false;
                }
                catch (TimeoutException e)
                {
                    Console.WriteLine("Connection Timeout");
                    success = false;
                }
            }

            return success;
        }//end of validate
    }//end of WebConn class
}
