﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace HealthTracker
{

    public class User
    {
        public User() { }
        public String username { get; set; }
        public String password { get; set; }
        public String alias { get; set; }
        public int height { get; set; }
        public bool autologin { get; set; }


    }//end of User class
}
