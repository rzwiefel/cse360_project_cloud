﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthTracker
{
    static class Program
    {

        public static String username {get; set;}
        public static bool authorized { get; set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());

            if (authorized)
            {
                Application.Run(new MainForm(username));
            }

        }//end of Main
    }
}
